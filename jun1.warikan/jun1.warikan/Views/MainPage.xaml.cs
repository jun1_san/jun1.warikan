﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace jun1.warikan.Views
{
    /// <summary>
    /// メインページ
    /// </summary>
    public partial class MainPage : ContentPage
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainPage()
        {
            InitializeComponent();
        }
    }
}